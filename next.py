#1.1.2
def dup(a):
    return str(a) * 2

def double_letter(my_str):
    return ''.join(map(dup, my_str))
	
#1.1.3
def div_four(x):
    return x % 4 == 0

def four_dividers(number):
    return list(filter(div_four, range(3, number)))

#1.1.4
def add(x, y):
    return int(x) + int(y) - int('0')

def sum_of_digits(number):
    return functools.reduce(add, list(str(number)))

#1.3.1
def intersection(list_1, list_2):
    return list(set([num for num in list_1 if num in list_2]))
	
#1.3.2
def is_prime(number):
    return len([i for i in range(2, number) if number % i == 0]) == 0

#1.3.3
def is_funny(string):
    return len([char for char in string if char not in ['a', 'h']]) == 0

#1.3.4
password = "sljmai ugrf rfc ambc: lglc dmsp mlc rum"

print(''.join([chr(ord(char) + 2) if char.isalpha() else char for char in password]))

#coin challenge
def combine_coins(coin, numbers):
    return ', '.join([coin + str(num) for num in numbers])

#1.5
#1
names = open('names.txt').read().split('\n')
print(max([i for i in names], key=len))

#2
names = open('names.txt').read()
print(len([char for char in names if char != '\n']))

#3
names = open('names.txt').read().split('\n')
print('\n'.join([i for i in names if len(i) == min(len(name) for name in names)]))

#4
names = open('names.txt').read().split('\n')
print('\n'.join([str(len(name)) for name in names]))

#5
names = open('names.txt').read().split('\n')
length = int(input("Enter name length: "))
print('\n'.join([name for name in names if len(name) == length]))

#2.2.2
class Capybara():
    def __init__(self, name, age):
        self.age = int(age)
        self.name = str(name)
    
    def birthday(self):
        self.age += 1
    
    def get_age(self):
        return self.age
    
def main():
    karius = Capybara("Karius" , 5)
    bactus = Capybara("Bactus", 8)

    bactus.birthday()
    print("Karius is " + str(karius.get_age()) + " years old")
    print("Bactus is " + str(bactus.get_age())+ " years old")
    
main()

#2.3.3
count_animals = 0
class Capybara():
    def __init__(self, age, name="Octavio"):
        self._age = int(age)
        self._name = str(name)
        global count_animals
        count_animals += 1
    
    def birthday(self):
        self._age += 1
    
    def get_age(self):
        return self._age
    
    def set_name(self, name):
        self._name = name
    
    def get_name(self):
        return self._name
    
def main():
    unnamed = Capybara(age=5)
    named = Capybara(8, "Bactus")
    
    print("Unnamed: ", unnamed.get_name())
    print("Named: ", named.get_name())
    
    unnamed.set_name("Capybarius")
    print("Unnamed: ", unnamed.get_name())
    
    print(count_animals)

main()

#2.3.4
class Pixel():
    #1
    def __init__(self, x=0, y=0, red=0, green=0, blue=0):
        self._x = x
        self._y = y
        self._red = red
        self._green = green
        self._blue = blue
    
    #2
    def set_coords(self, x, y):
        self._x = x
        self._y = y
    
    def set_grayscale(self):
        self._red = self._green = self._blue = (self._red + self._green + self._blue) // 3
    
    #3
    def print_pixel_info(self):
        print("X:", self._x, "Y:", self._y, "Color:", (self._red, self._green, self._blue), 
              "Green" if self._green != 0 and self._blue == self._red == 0 else
              "Red" if self._red != 0 and self._green == self._blue == 0 else
              "Blue" if self._blue != 0 and self._green == self._red == 0 else "")
    
def main():
    pixel = Pixel(5, 6, 250)
    pixel.print_pixel_info()
    pixel.set_grayscale()
    pixel.print_pixel_info()

main()

#2.4.2

#1
class BigThing():
    def __init__(self, var):
        self._var = var
    
    def size(self):
        if type(self._var) == int:
            return self._var
        return len(self._var)

#2
class BigCat(BigThing):
    def __init__(self, name, weight):
        BigThing.__init__(self, weight)
        self._name = name
        self._weight = weight
        print("Very Fat" if weight > 20 else "Fat" if weight > 15 else "OK")


#2.5
# Class that represents an animal
class Animal():
    zoo_name = "Hayaton"
    
    """Animal generic constructor function
    :param name: the name of the animal
    :param hunger: how hungry the animal is
    """
    def __init__(self, name, hunger=0):
        self._name = name
        self._hunger = hunger
    
    """ Name getter function
    :rtype: str
    """
    def get_name(self):
        return self._name
    
    """ Checks if the animal is hungry
    :rtype: bool
    """
    def is_hungry(self):
        return self._hunger > 0
    
    # Lower the animal's hunger level by one
    def feed(self):
        self._hunger -= 1
      
    # Print the animal's noise
    def talk(self):
        pass
    
    """ Type getter
    :rtype: str
    """
    def get_type(self):
        return self._type

# Class representing a dog using the Animal class
class Dog(Animal):
    _type = "Dog"
    
    # Print the dog's noise
    def talk(self):
        print("woof woof")
    
    # Print the dog's second voice line
    def fetch_stick(self):
        print("There you go, sir!")
    
# Class representing a cat using the Animal class    
class Cat(Animal):
    _type = "Cat"
    
    # Print the cat's noise
    def talk(self):
        print("meow")
    
    # Print the cat's second voice line
    def chase_laser(self):
        print("Meeeeow")
    
# Class representing a skunk using the Animal class 
class Skunk(Animal):
    _stink_count=6
    _type = "Skunk"
    
    # Print the skunk's noise
    def talk(self):
        print("tsssss")
    
    # Print the skunk's second voice line
    def stink(self):
        print("Dear lord!")
    

# Class representing a unicorn using the Animal class
class Unicorn(Animal):
    _type = "Unicorn"
    
    # Print the unicorn's noise
    def talk(self):
        print("Good day, darling")
    
    # Print the unicorn's second voice line
    def sing(self):
        print("I’m not your toy...")
    
# Class representing a dragon using the Animal class        
class Dragon(Animal):
    _color="Green"
    _type = "Dragon"
    
    # Print the dragon's noise
    def talk(self):
        print("Raaaawr")
    
    # Print the dragon's second voice line
    def breath_fire(self):
        print("$@#$#@$")
    

# Main function
def main():
    zoo_lst = [Dog("Brownie", 10), Cat("Zelda", 3), Skunk("Stinky"), Unicorn("Keith", 7), 
               Dragon("Lizzy", 1450), Dog("Doggo", 80), Cat("Kitty", 80), Skunk("Stinky Jr.", 80), 
               Unicorn("Clair", 80), Dragon("McFly", 80)]
    
    for animal in zoo_lst:
        while animal.is_hungry():
            animal.feed()
    
    for animal in zoo_lst:
        print(animal.get_type(), animal.get_name())
        animal.talk()
        if isinstance(animal, type(Dog("name"))):
            animal.fetch_stick()
        elif isinstance(animal, type(Cat("name"))):
            animal.chase_laser()
        elif isinstance(animal, type(Skunk("name"))):
            animal.stink()
        elif isinstance(animal, type(Unicorn("name"))):
            animal.sing()
        elif isinstance(animal, type(Dragon("name"))):
            animal.breath_fire()
    print(zoo_lst[0].zoo_name)
    
main()

#3.1.3
def stop_iteration_error(my_list):
    my_iter = iter(my_list)
    for item in my_iter:
        pass
    next(my_iter)

def zero_division_error():
    print(1/0)

def assertion_error():
    y = 0
    assert y != 0, "Error Message"
    print(1/y)

def import_error():
    from random import hello
    
def key_error():
    my_dict = {"hey": 1, "hey1": 2}
    my_dict["hello"]
    
def syntax_error():
    print()"hey"

def indentation_error():
    if (1):
    print("hey")

def type_error():
    print(3.14 * 'hey')
   
#3.2.5
def read_file(file_name):
    ret_str = "__CONTENT_START__ /n"
    try:
        ret_str += open(file_name).read()
    except:
        ret_str += "__NO_SUCH_FILE__"
    finally:
        ret_str += "__CONTENT_END__"
    return ret_str
    
#3.3.2
class UnderAgeException(Exception):
    def __init__(self, age):
        self._age = age
    def __str__(self):
        return "The person is underage. They are " + str(self._age) + " years old. They can join in " + str(18 - self._age) + " years."

def send_invitation(name, age):
    if int(age) < 18:
        raise UnderAgeException(age)
    else:
        print("You should send an invite to " + name)

#3.4
import string

class UsernameContainsIllegalCharacter(Exception):
    def __init__(self, char, index):
        self._char = char
        self._index = index
    def __str__(self):
        return "The username contains an illegal character " + '"' + str(self._char) + '"' + " at index " + str(self._index)

class UsernameTooShort(Exception):
    def __str__(self):
        return "The username is too short"

class UsernameTooLong(Exception):
    def __str__(self):
        return "The username is too long"

class PasswordMissingCharacter(Exception):
    def __str__(self):
        return "The password is missing a character"

class PasswordMissingLower(PasswordMissingCharacter):
    def __str__(self):
        return "The password is missing a character (Lowercase)"

class PasswordMissingUpper(PasswordMissingCharacter):
    def __str__(self):
        return "The password is missing a character (Uppercase)"

class PasswordMissingDigit(PasswordMissingCharacter):
    def __str__(self):
        return "The passwword is missing a character(Digit)"

class PasswordMissingSpecial(PasswordMissingCharacter):
    def __str__(self):
        return "The password is missing a character (Special)"

class PasswordTooShort(Exception):
    def __str__(self):
        return "The password is too short"

class PasswordTooLong(Exception):
    def __str__(self):
        return "The password is too long"

def check_input(username, password):
    if len(username) < 3:
        raise UsernameTooShort()
    
    if len(username) > 16:
        raise UsernameTooLong()
    
    for i in range(len(username)):
        if not(username[i].isalpha() or username[i] in '0123456789_'):
            raise UsernameContainsIllegalCharacter(username[i], i)    

    if len(password) < 8:
        raise PasswordTooShort()
    
    if len(password) > 40:
        raise PasswordTooLong()
    
    contains_lower = False
    contains_upper = False
    contains_number = False
    contains_punc = False
    
    for i in password:
        if i in string.punctuation:
            contains_punc = True
        elif i.isalpha():
            if i.islower():
                contains_lower = True
            elif i.isupper():
                contains_upper = True
        elif i in '0123456789':
                contains_number = True
    
    if not(contains_lower and contains_upper and contains_number and contains_punc):
        if not contains_lower:
            raise PasswordMissingLower()
        if not contains_upper:
            raise PasswordMissingUpper()
        if not contains_number:
            raise PasswordMissingDigit()
        if not contains_punc:
            raise PasswordMissingSpecial()
        
    print("OK")


def main():
    check_input("A_1", "abcdefghijklmnop")

    username = str(input("Enter a username"))
    password = str(input("Enter a password"))
    
    check_input(username, password)
    

main()


#4.1.2
def translate(sentence):
    words = {'esta': 'is', 'la': 'the', 'en': 'in', 'gato': 'cat', 'casa': 'house', 'el': 'the'}
    return ' '.join(words[i] for i in sentence.split(' '))
    
    
#4.1.3
import math
def is_prime(n):
    if n <= 1:
        return False
    for j in range(2, int(math.sqrt(n)) + 1):
        if n % j == 0:
            return False
    return True
    
    
def first_prime_over(n):
    prime_generator = (i for i in range(n + 1, 2**30) if is_prime(i))
    return next(prime_generator)
 
 
#4.2.2
def parse_ranges(ranges_string):
    ranges = ranges_string.split(',')
    pairs = [[(int(i.split('-')[0])), int(i.split('-')[1])+1] for i in ranges]
    return (num for start, end in pairs for num in range(start, end))


#4.3.4
def get_fibo():
    first = 0
    second = 1
    
    while True:
        yield first
        
        second = first + second
        first = second - first
        
#4.4
def gen_secs():
    seconds = 0
    while True:
        yield seconds % 60
        seconds += 1

def gen_minutes():
    minutes = 0
    while True:
        yield minutes % 60
        minutes += 1

def gen_hours():
    hours = 0
    while True:
        yield hours % 24
        hours += 1

def gen_time():
    sec_gen = gen_secs()
    min_gen = gen_minutes()
    hours_gen = gen_hours()
    
    hours = minutes = seconds = 0
    while True:
        yield (hours, minutes, seconds)
        
        seconds = next(sec_gen)
        if seconds == 0:
            minutes = next(min_gen)
            
            if minutes == 0:
                hours = next(hours_gen)

def gen_years(start=2023):
    year = start
    while True:
        yield year
        year += 1

def gen_months():
    month = 0
    while True:
        yield month % 12 + 1
        month += 1

def gen_days(month, leap_year=True):
    if month == 2:
        if leap_year:
            yield 29
        else:
            yield 28
    if month in (1, 3, 5, 7, 8, 10, 12):
        yield 31
    else:
        yield 30
        
def gen_date():
    year_gen = gen_years()
    month_gen = gen_months()
    day_gen = gen_days(1)
    
    time_gen = gen_time()
    
    year = next(year_gen)
    month = next(month_gen)
    day = 1
    days_in_month = next(gen_days(month, year % 4 == 0 and year % 100 != 0))
    
    time = next(time_gen)
    
    while True:
        yield (day, month, year, time[0], time[1], time[2])
        
        time = next(time_gen)
        
        if time[0] == time[1] == time[2] == 0:
            day += 1
            
            if day > days_in_month:
                day = 1
                month = next(month_gen)
                next(gen_days(month, year % 4 == 0 and year % 100 != 0))
                
                if month == 1:
                    year = next(year_gen)

                    
gd = gen_date()

i = 0
while True:
    if i % 1000000 == 0:
        print("%02d/%02d/%04d %02d:%02d:%02d" % next(gd))
    i += 1
    next(gd)
    
    
#5.1.2
import winsound

freqs = {"la": 220,
         "si": 247,
         "do": 261,
         "re": 293,
         "mi": 329,
         "fa": 349,
         "sol": 392,
         }

notes = "sol,250-mi,250-mi,500-fa,250-re,250-re,500-do,250-re,250-mi,250-fa,250-sol,250-sol,250-sol,500"

notes = notes.split('-')
for note in notes:
    note = note.split(',')
    winsound.Beep(freqs[note[0]], int(note[1]))

#5.2.2
numbers = iter(list(range(1, 101)))
for i in numbers:
    print(i)
    next(numbers)
    next(numbers)
    
#5.2.3
from itertools import combinations

bills = [20, 20, 20, 10, 10, 10, 10, 10, 5, 5, 1, 1, 1, 1, 1]

cnt = 0

for i in range(1, len(bills)):
    p = combinations(bills, i)
    p = set(list(p))
    for j in list(p):
        if sum(j) == 100:
            print(j)
            cnt += 1
print(cnt)

#5.3.2
class MusicNotes():
    freqs = {
        "la": 55.0,
        "si": 61.74,
        "do": 65.41,
        "re": 73.42,
        "mi": 82.41,
        "fa": 87.31,
        "sol": 98.0,
    }

    def __init__(self):
        self.octave = 0
        self.index = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self.octave <= 4:
            notes = list(MusicNotes.freqs.values())
            current_note = notes[self.index % len(notes)]
            frequency = current_note * (2 ** self.octave)
            self.index += 1
            if self.index >= len(notes):
                self.index = 0
                self.octave += 1
            return frequency
        else:
            raise StopIteration()

notes_iter = iter(MusicNotes())
for freq in notes_iter:
    print(freq)
    
#5.4

"""Function that checks if ID is valid
:param id_number: ID Number
:rtype: bool
"""
def check_id_valid(id_number):
    num_str = str(id_number)
    return sum([((i % 2 + 1) * int(num_str[i])) // 10 + ((i % 2 + 1) * int(num_str[i])) % 10 for i in range(len(num_str))]) % 10 == 0

# ID iterator class
class IDIterator():
    def __init__(self, id):
        self._id = id
    
    def __iter__(self):
        return self
"""Returns next valid ID
:rtype: int
"""    
    def __next__(self):
        while self._id <= 999999999:
            self._id += 1            
            if check_id_valid(self._id):
                return self._id
                     
        raise StopIteration

"""ID generator function
:param id_number: ID Number
:rtype: int
"""
def id_generator(id_number):
    while True:
        id_number += 1
        if id_number > 999999999:
            raise StopIteration()
        if check_id_valid(id_number):
            yield id_number
            
# Program main function
def main():
    id_number = int(input("Enter ID: "))
    gen_it = str(input("Generator or Iterator? (gen/it)? "))
    
    example = None
    
    if gen_it == "it":
        example = iter(IDIterator(id_number))
    elif gen_it == "gen":
        example = id_generator(id_number)
    
    for _ in range(10):
        print(next(example))

main()

#6.1.3
import tkinter as tk

def display_image():
    image = tk.PhotoImage(file="favorite_video.png")
    image_label = tk.Label(window, image=image)
    image_label.pack()
    image_label.image = image

window = tk.Tk()
window.title("Image Viewer")

label = tk.Label(window, text="what's my favorite video?", fg="pink", font=20)
label.pack()

button = tk.Button(window, text="click to find out!", command=display_image)
button.pack()

window.mainloop()

#6.2.5
#file1
class GreetingCard:
    def __init__(self, recipient="Dana Ev", sender="Eyal Ch"):
        self._recipient = recipient
        self._sender = sender

    def greeting_msg(self):
        print("sender", self._sender, "recipient", self._recipient)
        
#file2
from file1 import GreetingCard

class BirthdayCard(GreetingCard):
    def __init__(self, recipient, sender, sender_age=0):
        GreetingCard.__init__(self, recipient, sender)
        self._sender_age = sender_age
    def greeting_msg(self):
        print("sender", self._sender, "recipient", self._recipient, "Happy Birthday!", self._sender_age)

#main
from file2 import BirthdayCard
BirthdayCard("a", "b", 5).greeting_msg()

#6.3.3
from gtts import gTTS as speak
import os

message = "first time i'm using a package in next.py course"

language = "en"

voice_msg = speak(text=message, lang=language)
voice_msg.save("first.mp3")

os.system("first.mp3")

#6.4
existing_image = Image.open("ex6p4.jpg")

draw = ImageDraw.Draw(existing_image)
for i in range(0, len(first) - 3, 2):
    x1, y1 = first[i],first[i + 1]
    x2, y2 = first[i + 2], first[i + 3]
    draw.line((x1, y1, x2, y2), fill="black", width=5)

for i in range(0, len(second) - 3, 2):
    x1, y1 = second[i], second[i + 1]
    x2, y2 = second[i + 2], second[i + 3]
    draw.line((x1, y1, x2, y2), fill="black", width=5)

existing_image.save("output.jpg")